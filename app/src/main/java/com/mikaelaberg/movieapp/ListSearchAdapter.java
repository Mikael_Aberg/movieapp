package com.mikaelaberg.movieapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Mikael on 2016-01-27.
 */
public class ListSearchAdapter extends BaseAdapter {

    private Context context;
    private Picasso picasso;
    private ArrayList<String> names, types, images;
    private LayoutInflater inflater;

    public ListSearchAdapter(Context context, ArrayList<String> names, ArrayList<String> types, ArrayList<String> images) {
        this.context = context;
        this.names = names;
        this.types = types;
        this.images = images;

        picasso = Picasso.with(context);
        picasso.setIndicatorsEnabled(true);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.name = (TextView) convertView.findViewById(R.id.firstLine);
            viewHolder.type = (TextView) convertView.findViewById(R.id.secondLine);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (images.get(position).equals("N/A")) {
            picasso.load(R.drawable.no_photo_avalible).into(viewHolder.imageView);
        } else {
            picasso.load(images.get(position)).placeholder(R.drawable.loading).error(R.drawable.file_not_found).into(viewHolder.imageView);
        }

        viewHolder.name.setText(names.get(position));
        viewHolder.type.setText(types.get(position));

        return convertView;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView name;
        TextView type;
    }
}
