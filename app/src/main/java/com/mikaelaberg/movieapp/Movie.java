package com.mikaelaberg.movieapp;

import java.util.ArrayList;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class Movie {

    @Getter
    @Setter
    private Date released;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String rated;
    @Getter
    @Setter
    private String plot;

    @Getter
    private ArrayList<String> genre;
    @Getter
    private ArrayList<String> actors;
    @Getter
    private ArrayList<String> directors;
    @Getter
    private ArrayList<String> writers;
    @Getter
    private ArrayList<String> languages;

    @Getter
    @Setter
    private int year;
    @Getter
    @Setter
    private int runtime;
    @Getter
    @Setter
    private int type;
    @Getter
    @Setter
    private double metaScore;
    @Getter
    @Setter
    private double imdbRating;

}
