package com.mikaelaberg.movieapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ItemDetailActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_detail_layout);

        Button btn = (Button)findViewById(R.id.exitButton);

        Log.e("Movie: ", getIntent().getExtras().getString("movie"));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemDetailActivity.this.finish();
            }
        });
    }
}
