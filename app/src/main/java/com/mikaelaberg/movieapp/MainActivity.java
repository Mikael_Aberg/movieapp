package com.mikaelaberg.movieapp;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private String search;
    private Point size;
    private Picasso picasso;
    ListView list;
    ArrayList<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        names = new ArrayList<>();

        setContentView(R.layout.list_layout);
        Button button = (Button) findViewById(R.id.search_button);

        picasso = Picasso.with(getApplicationContext());
        picasso.setIndicatorsEnabled(true);
        setSize();

        list = (ListView)findViewById(R.id.listView);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Intent intent = new Intent(getApplicationContext(), ItemDetailActivity.class);
                intent.putExtra("movie", names.get(position));
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText);
                search = editText.getText().toString();
                editText.setText("");
                new MyAsyncTask().execute();
            }
        });
    }

    private void setSize(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        size = new Point(displaymetrics.widthPixels, displaymetrics.heightPixels);
    }

    class MyAsyncTask extends AsyncTask<String, String, Void> {

        private ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        InputStream inputStream = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Downloading your data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface arg0) {
                    MyAsyncTask.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {

            HttpURLConnection urlConnection = null;

            StringBuilder string = new StringBuilder();

            for(Character c : search.toCharArray()){
                if(c.equals(' ')){
                    string.append('+');
                }else {
                    string.append(c);
                }
            }

            String url_select = "http://www.omdbapi.com/?s=" + string.toString() + "&r=json";

            try {

                URL url = new URL(url_select);

                urlConnection = (HttpURLConnection) url.openConnection();
                inputStream = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"),8);
                StringBuilder sBuilder = new StringBuilder();

                String line;
                while ((line = bReader.readLine()) != null) {
                    Log.e("Line", line);
                    sBuilder.append(line);
                }

                inputStream.close();
                result = sBuilder.toString();

            } catch (Exception e) {
                Log.e("StringBuilding", "Error converting result " + e.toString());
            }

            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }
            return null;
        } // protected Void doInBackground(String... params)

        protected void onPostExecute(Void v) {
            //parse JSON data
            try {

                ArrayList<String> images = new ArrayList<>();
                ArrayList<String> type = new ArrayList<>();

                JSONObject firstJson = new JSONObject(result);
                JSONArray jsonArray = firstJson.getJSONArray("Search");

                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);

                    images.add(object.getString("Poster"));
                    names.add(object.getString("Title"));
                    type.add(object.getString("Type"));

                }
                list.setAdapter(new ListSearchAdapter(getApplicationContext(), names, type, images));

            this.progressDialog.dismiss();

            } catch (JSONException e) {
                Log.e("JSONException", "Error: " + e.toString());
            } // catch (JSONException e)
        } // protected void onPostExecute(Void v)
    } //class MyAsyncTask extends AsyncTask<String, String, Void>
}